﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponScript : MonoBehaviour
{

    [SerializeField] protected int weaponDamage;
    [SerializeField] protected float weaponSpeed;
    [SerializeField] protected float bulletSpeed;
    [SerializeField] public GameObject bullet;
    protected BulletScript BulletScript;

    public float GetWeaponSpeed()
    {
        return weaponSpeed;
    }

    void Start()
    {
    }

    public virtual void Shoot()
    {
        
        
    }

    public virtual void Effect()
    {
        
    }
}
