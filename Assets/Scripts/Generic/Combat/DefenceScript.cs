﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefenceScript : MonoBehaviour {

    protected PlayerScript player;
    [SerializeField]
    protected float defence = 100;
    [SerializeField]
    protected bool degradable = true; //does it degrade with every hit?
    [SerializeField]
    protected float regentime = 2; //how fast would it regen.
    protected float backupDefence; //origonal defence stored 
    protected float elipesTime = 0.0f;
    public void Awake()
    {
        player = gameObject.GetComponent<PlayerScript>();
        player.AddDefence(this);
    }

    public virtual int dealDamage(int damage)
    {
        if (degradable)
        {
            damage = Mathf.FloorToInt(damage - defence);
            defence -= defence*0.10f;
            if (defence <= 0.0f)
            {
                elipesTime = regentime;
            }
            return damage;
        }
        else
        {
            damage -= Mathf.FloorToInt(defence*0.75f);
        }
        return damage;
    }

    public virtual void UpdatingFunct()
    {
        if (!(elipesTime > 0)) return;
        elipesTime -= Time.deltaTime;
        if (elipesTime < 0.0f)
        {
            defence += backupDefence / regentime;
        }
    }

    void Update()
    {
       UpdatingFunct(); 
    }
    public virtual void JumpDam()
    {
        
    }

}
