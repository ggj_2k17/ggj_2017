﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostBullet : BulletScript {

	public void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.CompareTag("player1") || collision.CompareTag("player2")) { Debug.Log("Got hit"); collision.gameObject.GetComponent<PlayerScript>().DealDamage(bulletDamage); Destroy(gameObject); }
    }
}
