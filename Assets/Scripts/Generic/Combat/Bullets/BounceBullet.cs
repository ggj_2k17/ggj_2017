﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BounceBullet : BulletScript {

	public override void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("wall")) Bounce(collision);
        if (collision.collider.CompareTag("player1") || collision.collider.CompareTag("player2")) {  collision.gameObject.GetComponent<PlayerScript>().DealDamage(bulletDamage); Destroy(gameObject); }
        if (collision.collider.tag == "destructwall") collision.gameObject.GetComponent<destructionController>().split();
    }

    private void Bounce(Collision2D collision)
    {
        var bounce = Vector2.Reflect(collision.relativeVelocity, collision.contacts[0].normal);
        bounce.Normalize();
        GetComponent<Rigidbody2D>().AddForce(bounce*5, ForceMode2D.Impulse);
        bulletDamage -= bulletDamage/2;
    }
}
