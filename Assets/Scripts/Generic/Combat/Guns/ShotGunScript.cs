﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotGunScript : WeaponScript
{

    [SerializeField] private int bulletsPerShot;

    public override void Shoot()
    {
        for (int i = 0; i < bulletsPerShot; i++)
        {
            var bull = Instantiate(bullet);
            BulletScript = bull.GetComponent<BulletScript>();
            BulletScript.forward = transform.position - transform.parent.transform.position;
            bull.transform.position = transform.position + BulletScript.forward*3;
            bull.transform.position += new Vector3(Random.value, Random.value, Random.value);
            BulletScript.SetSpeed(bulletSpeed);
            BulletScript.SetDamage(weaponDamage);
            BulletScript.Move();
        }
    }
	
}
