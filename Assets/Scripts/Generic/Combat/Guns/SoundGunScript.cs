﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundGunScript : WeaponScript {

    public override void Shoot()
    {
        var bull= Instantiate(bullet);
        BulletScript = bull.GetComponent<BulletScript>();
        BulletScript.forward = transform.position - transform.parent.transform.position;
        bull.transform.position = transform.position + BulletScript.forward*3;
        
        BulletScript.SetSpeed(bulletSpeed);
        BulletScript.SetDamage(weaponDamage);
        BulletScript.Move();
    }
}
