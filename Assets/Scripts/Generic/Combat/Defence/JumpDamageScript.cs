﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpDamageScript : DefenceScript
{

    private int jumpDamage = 10;

	public override int dealDamage(int damage)
	{
	    return damage;
	}

    public override void JumpDam()
    {
         player.DealDamage(jumpDamage);
    }
}
