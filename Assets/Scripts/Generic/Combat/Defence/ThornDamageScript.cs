﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThornDamageScript : DefenceScript
{
    [SerializeField] private int thornDamage = 20;

    public override int dealDamage(int damage)
    {
        return damage;
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("player1") || collision.collider.CompareTag("player2")) { collision.gameObject.GetComponent<PlayerScript>().DealDamage(thornDamage);}
    }
}

