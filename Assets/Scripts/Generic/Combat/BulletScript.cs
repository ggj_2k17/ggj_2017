﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    public enum size { mini, normal, max}
    [SerializeField] public size BallSize = size.normal;
    private float miniBall = 1.5f;
    private float maxBall = 0.8f;
    protected int bulletDamage;
    protected float bulletSpeed;
    public Vector3 forward;

    void Start()
    {
        if(BallSize == size.mini) transform.localScale = transform.localScale / miniBall;
        else if (BallSize == size.max) transform.localScale = transform.localScale/maxBall;
    }

    public void SetDamage(int damage)
    {
        bulletDamage = damage;
    }

    public void SetSpeed(float speed)
    {
        bulletSpeed = speed;
    }

    public virtual void Move()
    {
        GetComponent<Rigidbody2D>().AddForce( forward*bulletSpeed,ForceMode2D.Impulse);
        Destroy(gameObject,10);
    }

    public virtual void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.collider.CompareTag("wall")) Destroy(gameObject);
        if(collision.collider.CompareTag("player1") || collision.collider.CompareTag("player2")) {Debug.Log("Got hit"); collision.gameObject.GetComponent<PlayerScript>().DealDamage(bulletDamage); Destroy(gameObject); }
        if (collision.collider.tag == "destructwall") collision.gameObject.GetComponent<destructionController>().split();
    }
}
