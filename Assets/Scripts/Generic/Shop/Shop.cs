﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;
using OurLibrary.Events;
using UnityEngine.EventSystems;
using UnityEngine;

namespace RandomGame.UI
{
    public class Shop : MonoBehaviour
    {
        [SerializeField]
        private CanvasGroup canvasPl1;
        [SerializeField]
        private CanvasGroup canvasPl2;

        [SerializeField]
        private Image overlayPl1;
        [SerializeField]
        private Image overlayPl2;

        [SerializeField]
        private ToggleGroup togglesPl1;
        [SerializeField]
        private ToggleGroup togglesPl2;

        bool shoppingFinished = false;
        private int activePl = 0;
        private EventDispatcher _dispatcher = EventDispatcher.GetInstance();

        public Toggle start1;
        public Toggle start2;

        public StandaloneInputModule inputModule;
        public EventSystem eventSystem;
        

        private void Awake()
        {
            _dispatcher.AddEventListener(HUDEvents.OPEN_SHOP, openCorrectShop);
            
        }

        private void activateControlsToPlayer2()
        {
            eventSystem.SetSelectedGameObject(start2.gameObject);
            inputModule.verticalAxis = "P2LSVertical";
            inputModule.horizontalAxis = "P2LSHorizontal";
            inputModule.submitButton = "P2AButton";
            inputModule.cancelButton = "P2BButton";
        }

        private void activateControlsToPlayer1()
        {
            eventSystem.SetSelectedGameObject(start1.gameObject);
            inputModule.verticalAxis = "P1LSVertical";
            inputModule.horizontalAxis = "P1LSHorizontal";
            inputModule.submitButton = "P1AButton";
            inputModule.cancelButton = "P1BButton";
        }

        private void openCorrectShop(string type, object data)
        {
            shoppingFinished = false;
            canvasPl1.alpha = 1;
            canvasPl2.alpha = 1;
            int _player = (int)data;
            switch (_player)
            {
                case 1:
                    activePl = 2;
                    canvasPl1.interactable = false;
                    canvasPl2.interactable = true;
                    overlayPl1.enabled = true;
                    activateControlsToPlayer2();
                    break;
                case 2:
                    activePl = 1;
                    canvasPl2.interactable = false;
                    canvasPl1.interactable = true;
                    overlayPl2.enabled = true;
                    activateControlsToPlayer1();
                    break;
                default:
                    break;
            }
        }

        private void resetAll()
        {
            activePl = 0;
            canvasPl2.interactable = false;
            canvasPl2.alpha = 0;
            overlayPl2.enabled = false;
            canvasPl1.alpha = 0;
            canvasPl1.interactable = false;
            overlayPl1.enabled = false;
            shoppingFinished = false;
            togglesPl2.ActiveToggles().FirstOrDefault<Toggle>().isOn = false;
            togglesPl1.ActiveToggles().FirstOrDefault<Toggle>().isOn = false; 
            _dispatcher.Dispatch(HUDEvents.CLOSED_SHOP, true);
        }

        public void OnDoneButtonPressed()
        {
            if (!shoppingFinished)
            {
                if (activePl == 1) { 
                    if (!togglesPl1.AnyTogglesOn())
                        return;

                    Toggle selectedToggle = togglesPl1.ActiveToggles().FirstOrDefault<Toggle>();

                    _dispatcher.Dispatch(selectedToggle.name, activePl);
                    canvasPl1.interactable = false;
                    overlayPl1.enabled = true;

                    canvasPl2.interactable = true;
                    overlayPl2.enabled = false;
                    activateControlsToPlayer2();
                    activePl = 2;
                    shoppingFinished = true;
                }
                else
                {
                    if (!togglesPl2.AnyTogglesOn())
                        return;

                    Toggle selectedToggle = togglesPl2.ActiveToggles().FirstOrDefault<Toggle>();

                    _dispatcher.Dispatch(selectedToggle.name, activePl);
                    canvasPl1.interactable = true;
                    overlayPl1.enabled = false;

                    canvasPl2.interactable = false;
                    overlayPl2.enabled = true;
                    activateControlsToPlayer1();
                    activePl = 1;
                    shoppingFinished = true;
                }
            }else
            {
                if (activePl == 1)
                {
                    if (!togglesPl1.AnyTogglesOn())
                        return;

                    Toggle selectedToggle = togglesPl1.ActiveToggles().FirstOrDefault<Toggle>();
                    _dispatcher.Dispatch(selectedToggle.name, activePl);
                    resetAll();
                }
                else
                {
                    if (!togglesPl2.AnyTogglesOn())
                        return;

                    Toggle selectedToggle = togglesPl2.ActiveToggles().FirstOrDefault<Toggle>();
                    _dispatcher.Dispatch(selectedToggle.name, activePl);
                    resetAll();
                }
            }
        }
        private void disablePlayer1()
        {
            Input.GetKey("P1AButton");
        }
    }
}
