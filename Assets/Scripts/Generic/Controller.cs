﻿using UnityEngine;

public static class Controller
{

    public static PlayerController P1 = new PlayerController(1);
    public static PlayerController P2 = new PlayerController(2);

}

public class PlayerController 
{
    public float type;

    public PlayerController(float _type)
    {
        type = _type;
    }

    public float Triggers
    {
        get { return Input.GetAxis("P" + type + "Triggers"); }
    }

    public float LeftStickHorizontal
    {
        get { return Input.GetAxis("P" + type + "LSHorizontal"); }
    }

    public float LeftStickVertical
    {
        get { return Input.GetAxis("P" + type + "LSVertical"); }
    }

    public bool LeftStickClick
    {
        get { return Input.GetButtonDown("P" + type + "ThumbLeft"); }
    }

    public float RightStickHorizontal
    {
        get { return Input.GetAxis("P" + type + "RSHorizontal"); }
    }

    public float RightStickVertical
    {
        get { return Input.GetAxis("P" + type + "RSVertical"); }
    }

    public bool RightStickClick
    {
        get { return Input.GetButtonDown("P" + type + "ThumbRight"); }
    }

    public float DpadHorizontal
    {
        get { return Input.GetAxis("P" + type + "Dpad Horizontal"); }
    }

    public float DpadVertical
    {
        get { return Input.GetAxis("P" + type + "Dpad Vertical"); }
    }

    public float LeftTrigger
    {
        get { return Input.GetAxis("P" + type + "Left Trigger"); }
    }

    public bool LeftBumper
    {
        get { return Input.GetButtonDown("P" + type + "Left Bumper"); }
    }

    public float RightTrigger
    {
        get { return Input.GetAxis("P" + type + "Right Trigger"); }
    }

    public bool RightBumper
    {
        get { return Input.GetButtonDown("P" + type + "Right Bumper"); }
    }

    public bool AButton
    {
        get { return Input.GetButtonDown("P" + type + "AButton"); }
    }

    public bool BButton
    {
        get { return Input.GetButtonDown("P" + type + "BButton"); }
    }

    public bool XButton
    {
        get { return Input.GetButtonDown("P" + type + "XButton"); }
    }

    public bool YButton
    {
        get { return Input.GetButtonDown("P" + type + "YButton"); }
    }

    public bool SelectButton
    {
        get { return Input.GetButtonDown("P" + type + "SelectButton"); }
    }

    public bool StartButton
    {
        get { return Input.GetButtonDown("P" + type + "StartButton"); }
    }

}
