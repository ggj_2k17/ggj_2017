﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using RandomGame.UI.Effects;
using OurLibrary.Events;
using System;

namespace RandomGame.UI
{
    public class UIManager : MonoBehaviour
	{
       
        private EventDispatcher _dispatcher = EventDispatcher.GetInstance();

        // HUD variables
        [SerializeField]
        private Slider healthPl1;
        [SerializeField]
        private Slider healthPl2;
        [SerializeField]
        private Text endRound;


        private void Awake()
        {
            if (!healthPl1)
                Debug.Log("WARNING: player 1 health NULL");

            if (!healthPl2)
                Debug.Log("WARNING: player 2 health NULL");

            _dispatcher.AddEventListener(HUDEvents.HEALTH_CHANGED, updateHealth);
            
        }

        void Update()
        {
            if (Input.GetKey(KeyCode.Tab))
            {
                HUDEvents.HealhtObject test = new HUDEvents.HealhtObject(1, 10);
                updateHealth("test", test);
            }
            if (Input.GetKey(KeyCode.A))
            {
                HUDEvents.HealhtObject test = new HUDEvents.HealhtObject(2, 10);
                updateHealth("test", test);
            }
            if (Input.GetKey(KeyCode.S))
            { 
                triggerEndRound("test", 2);
            }

        }

        private void triggerEndRound(string type, object data)
        {
            int _player = (int)data;
            endRound.text = "PLAYER " + _player + " WON";
            Fade.FadeText(20, endRound, this, false);
            _dispatcher.Dispatch(HUDEvents.OPEN_SHOP, _player);
        }

        private void updateHealth(string type, object data)
        {
            HUDEvents.HealhtObject _healthData = (HUDEvents.HealhtObject)data;
            switch (_healthData.Player) {
                case 1:
                    healthPl1.value = _healthData.Amount;
                    break;
                case 2:
                    healthPl2.value = _healthData.Amount;
                    break;
                default:
                    Debug.Log("WARNING: no player number given");
                    break;
            }  
        }
        public void LoadGame()
        {
            SceneManager.LoadScene("Level 1");
        }

        public void QuitGame()
        {
            Application.Quit();
        }
	}
}
