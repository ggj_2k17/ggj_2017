﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RandomGame.UI
{
    public class HUDEvents
    {
        public const string WEAPON_CHANGED = "WEAPON_CHANGED";
        public const string OPEN_SHOP = "OPEN_SHOP";
        public const string CLOSED_SHOP = "CLOSED_SHOP";
        public const string HEALTH_CHANGED = "HEALTH_CHANGED";

        public class HealhtObject
        {
            public int Player;
            public int Amount;
            public HealhtObject(int player = 0, int amount = 0)
            {
                Player = player;
                Amount = amount;
            }
        }
    }
}
