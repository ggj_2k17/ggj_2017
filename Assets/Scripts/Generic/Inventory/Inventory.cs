﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using OurLibrary.Events;


//TODO: create limit of items hold, so the mediator won't place items out of range
namespace RandomGame.Inventory
{
	public class Inventory : MonoBehaviour
	{

		private List<InventoryItem> _items = new List<InventoryItem>();
		public List<InventoryItem> Items { get { return this._items; }}

		private int _totalWeight = 0;
		public int TotalSize { get{
                int size = 0;
                for (int i = 0; i < _items.Count; i++)
                {
                    size = size + _items[i].GetSize();
                }
                return size;
            } }

        public GameObject DeafaultItem = null;

		private EventDispatcher _dispatcher = EventDispatcher.GetInstance();

		public void AddItem(InventoryItem newItem)
		{   
            
			this._items.Add(newItem);
			//_dispatcher.Dispatch(InventoryEvents.ITEM_ADDED, newItem);
            SortInventory();
		}

        //TODO: create cheaper sorting system
        private void SortInventory()
        {
            List<InventoryItem> tempItems = _items;
            InventoryItem size;
            for (int i = tempItems.Count - 1; i >= 1; i--)
            {
                for (int j = 0; j < tempItems.Count - 1; j++)
                {
                    if (_items[j].GetSize() < tempItems[j + 1].GetSize())
                    {
                        size = tempItems[j]; 
                        tempItems[j] = tempItems[j + 1];
                        tempItems[j + 1] = size; 
                    }
                }
            }
            _items = tempItems;
        }


        //TODO: create function to remove items from the inventory
		public void RemoveItem(int id)
		{
			//this._items.Remove(id);
			_dispatcher.Dispatch(InventoryEvents.ITEM_REMOVED, id);
		}
	}
}
