﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using OurLibrary.Events;


namespace RandomGame.Inventory
{
	public class InventoryMediator : MonoBehaviour
	{
        
        public GameObject testImage;
        //inventory that holds all the item information
        public Inventory playerInventory;

        //items currently in the inventory
       // private List<Image> _items =  new List<Image>();
        
        //grid of dropzones
        private List<List<GameObject>> _grid = new List<List<GameObject>>();
        
        //holds the dropzonde art
        public GameObject GridImage;

        private const int SCALE = 100;
        public int TotalGridSize;

        public int gridX = 4;
        public int gridY = 8;
        private EventDispatcher _dispatcher = EventDispatcher.GetInstance();


        private void Awake()
        {
            FillInventoryTest();

            TotalGridSize = gridX * gridY;

            _dispatcher.AddEventListener(InventoryEvents.ITEM_ADDED, UpdateInventory);
            _dispatcher.AddEventListener(InventoryEvents.ITEM_REPLACED, manualItemPlacement);
            createGrid();
            fillInventory();
        }

        public void UpdateInventory(string type, object data)
        {
            fillInventory();
        }

        private void fillInventory()
        {
            for (int i = 0; i < playerInventory.Items.Count; i++)
            {
                InventoryItem currentItem = playerInventory.Items[i];
                if (currentItem.shownInInventory)
                    continue;

                Vector3 itemPos = new Vector3(0, 0, 0);

                //pathetic way to break the outer loop
                bool found = false;
                
                //put the item in the right position
                for (int x = 0; x < _grid.Count && !found; x++)
                {
                    for (int y = 0; y < _grid[x].Count; y++)
                    {
                        //find not occupied dropzone/gridtile
                        DropZone currentDropZone = _grid[x][y].GetComponent<DropZone>();
                        if (!currentDropZone.Occupied)
                        {
                            itemPos = _grid[x][y].transform.position;

                            //check if item fits, if it doesn't keep searching for a spot that does fit
                            if (checkItemSize(currentItem, new Vector2(x, y)))
                            {
                                currentDropZone.itemHeld = currentItem;

                                //if free position is found quit checking the loop
                                found = true;
                                break;
                            }
                        }
                    }
                }

                //add inventory item to the inventory
                GameObject newItem = Instantiate(currentItem.Thumb, itemPos, new Quaternion(0, 0, 0, 0), this.transform) as GameObject;
                RectTransform newItemTransform = newItem.transform as RectTransform;
                newItemTransform.sizeDelta = new Vector2(SCALE * currentItem.Size.x, SCALE * currentItem.Size.y);
                //currentItem.gameObject.AddComponent(MonoBehaviour, playerInventory.Items[i]);
                currentItem.shownInInventory = true;
            }
        }

        private void manualItemPlacement(string eventType, object data)
        {
            //object Adata = data;
        }

        private bool checkItemSize(InventoryItem item, Vector2 iterators)
        {
            //create list to check if the selected dropzone is not already occupied
            List<DropZone> potentialDropZones = new List<DropZone>(); 

            if (item.Size.y + iterators.y > gridY) {
                return false;
            }else{
                for (int i = 0; i < item.Size.y; i++)
                {
                    int currentIterator = (int)iterators.y + i;
                    DropZone currentDropZoneY = _grid[(int)iterators.x][(int)iterators.y + i].GetComponent<DropZone>();
                    potentialDropZones.Add(currentDropZoneY);

                    for (int j = 0; j < item.Size.x; j++)
                    {
                        DropZone currentDropZoneX = _grid[(int)iterators.x + j][currentIterator].GetComponent<DropZone>();
                        potentialDropZones.Add(currentDropZoneX);
                    }
                }
            }

            //when none of the potential dropzones is occupied by any other item it will give the potential dropzones an item to hold
            if (checkAvailability(potentialDropZones))
            {
                for (int i = 0; i < potentialDropZones.Count; i++)
                {
                    potentialDropZones[i].itemHeld = item;
                    //give item the dropzones it is occupying so it can release them when needed
                    item.DropZonesHeld = potentialDropZones;
                }
            }else
                return false;


            return true;
        }

        //check availability of droopzones
        private bool checkAvailability(List<DropZone> list)
        {
            for(int i = 0; i < list.Count; i++)
            {
                if (list[i].Occupied)
                    return false;
            }
            return true;
        }

        private void createGrid()
        {
            
            GameObject newTile = null;
            
            //create grid according to the specified gridsize
            for (int x = 0; x < gridX; x++)
            {
                List<GameObject> tempGrid = new List<GameObject>();
                for (int y = 0; y < gridY; y++)
                {
                   Vector3 newPos = new Vector3(this.transform.position.x + (x * SCALE), this.transform.position.y + (y * SCALE), 0);
                   newTile = Instantiate(GridImage, newPos, new Quaternion(0, 0, 0, 0), this.transform) as GameObject;
                   tempGrid.Add(newTile);
                }
                _grid.Add(tempGrid);
            }

            //give every dropzone its neighbours
            for (int x = 0; x < _grid.Count; x++)
            {
                for (int y = 0; y < _grid[x].Count; y++)
                {
                    DropZone currentDropZone = _grid[x][y].GetComponent<DropZone>();

                    if (y < gridY - 1)
                        currentDropZone.UpNeighbour = _grid[x][y + 1].GetComponent<DropZone>();

                    if (x < gridX - 1)
                        currentDropZone.RightNeighbour = _grid[x + 1][y].GetComponent<DropZone>();

                    if (y != 0)
                        currentDropZone.DownNeighbour = _grid[x][y - 1].GetComponent<DropZone>();

                    if (x != 0)
                        currentDropZone.LeftNeighbour = _grid[x - 1][y].GetComponent<DropZone>();
                }
            }
        }

        //for testing purposes
        private void FillInventoryTest()
        { 
            for (int i = 0; i < 15; i++)
            {
                InventoryItem newItem = new InventoryItem();
                newItem.SetID(i);
                newItem.Size = new Vector2(Random.Range(1,3), Random.Range(1, 3));

                //newItem.Description = "This is an item of type: " + ItemTypes.itemList[i];
                newItem.Name = "Harbinger";

                //newItem.Type = ItemTypes.itemList[i];
                newItem.Thumb = testImage;

                playerInventory.AddItem(newItem);
            }
        }

        //draw UI
        private void OnGUI()
		{
        }
	}
}
