﻿using UnityEngine;
using System.Collections;

namespace OurLibrary.Events
{
	public class InventoryEvents
	{
		public const string ITEM_ADDED = "InventoryItemAdded";
        public const string ITEM_REMOVED = "InventoryItemRemoved";
        public const string ITEM_REPLACED = "InventoryItemReplaced";
    }
}
