﻿using UnityEngine;
using System.Collections;

namespace RandomGame.Inventory
{
	public class ItemTypes
	{
		public const string WEAPON = "WEAPON";
		public const string ARMOR = "ARMOR";
		public const string POTION = "POTION";
		public const string READABLE = "READABLE";
		public const string JUNK = "JUNK";

		public static readonly string[] itemList = new string[5]{WEAPON, ARMOR, POTION, READABLE, JUNK};
	}
}
