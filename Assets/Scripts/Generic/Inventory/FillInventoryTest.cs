﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace RandomGame.Inventory
{
	public class FillInventoryTest : MonoBehaviour
	{
		public GameObject testImage;
		void Awake()
		{
            //fill inventory
            for (int i = 0; i < 15; i++)
            {
                InventoryItem newItem = new InventoryItem();
                newItem.SetID(i);
                newItem.Size = new Vector2(Random.Range(1,3), Random.Range(1, 3));


                //newItem.Description = "This is an item of type: " + ItemTypes.itemList[i];
                newItem.Name = "Harbinger";
                
                //newItem.Type = ItemTypes.itemList[i];
                newItem.Thumb = testImage;

                GetComponentInParent<Inventory>().AddItem(newItem);
            }
		}
	}
}
