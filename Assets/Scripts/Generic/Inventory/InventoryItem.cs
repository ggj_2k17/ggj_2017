﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;


namespace RandomGame.Inventory
{
	public class InventoryItem
	{
        public List<DropZone> DropZonesHeld = new List<DropZone>();

        private int _id;
		
		public int Weight = 0;
        public Vector2 Size = new Vector2(0,0);
        
		public string Name = "[NAME]";
		public string Description = "[DESCRIPTION]";
		public string Type = "[TYPE]";

        public bool shownInInventory = false;

		//image the item will have in inventory
		public GameObject Thumb;

		public int ID { get {return this._id;}}

		//functions for test purposes
		public void SetID(int newID)
		{
			this._id = newID;
		}

        public int GetSize()
        {
            return (int)(Size.x * Size.y);
        }
	}
}
