﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using OurLibrary.Events;

namespace RandomGame.Inventory
{
    public class DropZone : MonoBehaviour, IDropHandler {
        public DropZone RightNeighbour = null;
        public DropZone DownNeighbour = null;
        public DropZone UpNeighbour = null;
        public DropZone LeftNeighbour = null;

        public bool Occupied
        {
            get
            {
                if (itemHeld == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        public InventoryItem itemHeld = null;

        private EventDispatcher _dispatcher = EventDispatcher.GetInstance();

        public void OnDrop(PointerEventData data) {
            RaycastResult itemHolding = data.pointerPressRaycast;
            GameObject item = itemHolding.gameObject;
            
            object[] eventData = { item, this };
            _dispatcher.Dispatch(InventoryEvents.ITEM_REPLACED, eventData);
        }
    }
}
