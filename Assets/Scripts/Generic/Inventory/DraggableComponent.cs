﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class DraggableComponent : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

    private Transform originalParent = null;
    private Vector3 originalPosition = new Vector3(0,0,0);
    private Vector2 correctedPos = new Vector2(0, 0);

    public void OnBeginDrag (PointerEventData mouseData) {
        originalPosition = transform.position;
        originalParent = transform.parent;
        this.transform.SetParent(transform.parent.parent);
        
        //store the position min the pivot position to stop the item from jumping positions when dragged
        Vector2 originVec2 = new Vector2(originalPosition.x, originalPosition.y);
        correctedPos = originVec2 - mouseData.position;
    }

    public void OnDrag(PointerEventData mouseData)
    {
        this.gameObject.transform.position = mouseData.position + correctedPos;
    }

    public void OnEndDrag(PointerEventData mouseData)
    {
        this.transform.SetParent(originalParent);
        this.transform.position = originalPosition;
    }
}
