﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

namespace RandomGame.UI.Effects
{
    public class Fade
    {
        public static void FadeText(float seconds, Text text, MonoBehaviour manager = null, bool toZero = true)
        {
            
            if (!manager)
            {
                Debug.Log("WARNING: Fade is called but no manager send");
                return;
            }

            if (toZero)
                manager.StartCoroutine(fadeTextToZero(seconds, text));
            else
                manager.StartCoroutine(fadeTextToFull(seconds, text));
            
        }
        
        private static IEnumerator fadeTextToZero(float seconds, Text text)
        {
            text.color = new Color(text.color.r, text.color.g, text.color.b, 0);
            while (text.color.a < 1.0f)
            {
                text.color = new Color(text.color.r, text.color.g, text.color.b, text.color.a + (Time.deltaTime / seconds));
                yield return null;
            }

        }

        private static IEnumerator fadeTextToFull(float seconds, Text text)
        {
            text.color = new Color(text.color.r, text.color.g, text.color.b, 1);
            while (text.color.a > 0.0f)
            {
                text.color = new Color(text.color.r, text.color.g, text.color.b, text.color.a - (Time.deltaTime / seconds));
                yield return null;
            }
        }


        //TODO: create fade effects for other ui elements
    }
}
