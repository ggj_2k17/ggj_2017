﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerDebugScript : MonoBehaviour {

    void Update()
    {
        if (Controller.P1.LeftStickHorizontal > 0.19f || Controller.P1.LeftStickHorizontal < -0.19f) Debug.Log("P1 LeftStick Horizontal: " + Controller.P1.LeftStickHorizontal);
        if (Controller.P1.LeftStickVertical > 0.19f || Controller.P1.LeftStickVertical < -0.19f) Debug.Log("P1 LeftStick Vertical: " + Controller.P1.LeftStickVertical);
        if (Controller.P1.LeftStickClick ) Debug.Log("P1 LeftStick Click: " + Controller.P1.LeftStickClick);

        if (Controller.P1.RightStickHorizontal > 0.19f || Controller.P1.RightStickHorizontal < -0.19f) Debug.Log("P1 RightStick Horizontal: " + Controller.P1.RightStickHorizontal);
        if (Controller.P1.RightStickVertical > 0.19f || Controller.P1.RightStickVertical < -0.19f) Debug.Log("P1 RightStick Vertical: " + Controller.P1.RightStickVertical);
        if (Controller.P1.RightStickClick) Debug.Log("P1 RighStick Click: " + Controller.P1.RightStickClick);

        if (Controller.P1.DpadHorizontal > 0.19f || Controller.P1.DpadHorizontal < -0.19f) Debug.Log("P1 Dpad Horizontal: " + Controller.P1.DpadHorizontal);
        if (Controller.P1.DpadVertical > 0.19f || Controller.P1.DpadVertical < -0.19f) Debug.Log("P1 Dpad Vertical: " + Controller.P1.DpadVertical);

        if (Controller.P1.LeftTrigger > 0.19f) Debug.Log("P1 Left Trigger: " + Controller.P1.LeftTrigger);
        if (Controller.P1.LeftBumper) Debug.Log("P1 Left Bumper: " + Controller.P1.LeftBumper);

        if (Controller.P1.RightTrigger > 0.19f) Debug.Log("P1 Right Trigger: " + Controller.P1.RightTrigger);
        if (Controller.P1.RightBumper ) Debug.Log("P1 Right Bumper: " + Controller.P1.RightBumper);

        if (Controller.P1.AButton ) Debug.Log("P1 A Button: " + Controller.P1.AButton);
        if (Controller.P1.BButton ) Debug.Log("P1 B Button: " + Controller.P1.BButton);
        if (Controller.P1.XButton ) Debug.Log("P1 X Button: " + Controller.P1.XButton);
        if (Controller.P1.YButton ) Debug.Log("P1 Y Button: " + Controller.P1.YButton);
        if (Controller.P1.SelectButton ) Debug.Log("P1 Select Button: " + Controller.P1.SelectButton);
        if (Controller.P1.StartButton ) Debug.Log("P1 Start Button: " + Controller.P1.StartButton);

        if(Controller.P1.Triggers >0.19f) Debug.Log("P1 Tiggers right " + Controller.P1.Triggers);
        if (Controller.P1.Triggers < -0.19f) Debug.Log("P1 Tiggers left " + Controller.P1.Triggers);

        if (Controller.P2.LeftStickHorizontal > 0.19f || Controller.P2.LeftStickHorizontal < -0.19f) Debug.Log("P2 LeftStick Horizontal: " + Controller.P2.LeftStickHorizontal);
        if (Controller.P2.LeftStickVertical > 0.19f || Controller.P2.LeftStickVertical < -0.19f) Debug.Log("P2 LeftStick Vertical: " + Controller.P2.LeftStickVertical);
        if (Controller.P2.LeftStickClick) Debug.Log("P2 LeftStick Click: " + Controller.P2.LeftStickClick);

        if (Controller.P2.RightStickHorizontal > 0.19f || Controller.P2.RightStickHorizontal < -0.19f) Debug.Log("P2 RightStick Horizontal: " + Controller.P2.RightStickHorizontal);
        if (Controller.P2.RightStickVertical > 0.19f || Controller.P2.RightStickVertical < -0.19f) Debug.Log("P2 RightStick Vertical: " + Controller.P2.RightStickVertical);
        if (Controller.P2.RightStickClick ) Debug.Log("P2 RighStick Click: " + Controller.P2.RightStickClick);

        if (Controller.P2.DpadHorizontal > 0.19f || Controller.P2.DpadHorizontal < -0.19f) Debug.Log("P2 Dpad Horizontal: " + Controller.P2.DpadHorizontal);
        if (Controller.P2.DpadVertical > 0.19f || Controller.P2.DpadVertical < -0.19f) Debug.Log("P2 Dpad Vertical: " + Controller.P2.DpadVertical);

        if (Controller.P2.LeftTrigger > 0.19f) Debug.Log("P2 Left Trigger: " + Controller.P2.LeftTrigger);
        if (Controller.P2.LeftBumper) Debug.Log("P2 Left Bumper: " + Controller.P2.LeftBumper);

        if (Controller.P2.RightTrigger > 0.19f) Debug.Log("P2 Right Trigger: " + Controller.P2.RightTrigger);
        if (Controller.P2.RightBumper ) Debug.Log("P2 Right Bumper: " + Controller.P2.RightBumper);

        if (Controller.P2.AButton ) Debug.Log("P2 A Button: " + Controller.P2.AButton);
        if (Controller.P2.BButton ) Debug.Log("P2 B Button: " + Controller.P2.BButton);
        if (Controller.P2.XButton ) Debug.Log("P2 X Button: " + Controller.P2.XButton);
        if (Controller.P2.YButton ) Debug.Log("P2 Y Button: " + Controller.P2.YButton);
        if (Controller.P2.SelectButton ) Debug.Log("P2 Select Button: " + Controller.P2.SelectButton);
        if (Controller.P2.StartButton ) Debug.Log("P2 Start Button: " + Controller.P2.StartButton);

        if (Controller.P2.Triggers > 0.19f) Debug.Log("P2 Tiggers right " + Controller.P2.Triggers);
        if (Controller.P2.Triggers < -0.19f) Debug.Log("P2 Tiggers left " + Controller.P2.Triggers);

    }
}
