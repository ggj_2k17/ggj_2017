﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace OurLibrary.Events
{
	public delegate void GameEventDelegate(string type, object data);
	
	public class EventDispatcher
	{
		private static EventDispatcher _instance;
		private static Dictionary <string, List<GameEventDelegate>> _allEvents;

		public static EventDispatcher GetInstance()
		{
			if (_instance == null)
			{
				_instance = new EventDispatcher();
				_allEvents = new Dictionary<string, List<GameEventDelegate>>();
			}

			return _instance;
		}

		public void Dispatch(string type, object data = null)
		{
			List<GameEventDelegate> listeners = null;
			if (_allEvents.TryGetValue(type, out listeners) && listeners.Count > 0)
			{
				//invoke every listener
				for (int i = 0, length = listeners.Count; i < length; i++)
				{
					if (listeners[i] != null)
						listeners[i].Invoke(type, data);
				}
			}
			else
				Debug.Log("WARNING: event [" + type + "] has no listeners");
		}

		public void AddEventListener(string type, GameEventDelegate data)
		{
			List<GameEventDelegate> listeners = null;

			//check if this event type already has some listeners. if so, retrieve that list
			if (!_allEvents.TryGetValue(type, out listeners))
			{
				//if this event didn't contain listeners create a new list and add it to the global events list
				listeners = new List<GameEventDelegate>();
				_allEvents.Add(type, listeners);
			}

			//prevent double listeners for the same delegate
			if (!listeners.Contains(data))
				listeners.Add(data);
			
		}


		public void RemoveEventListener(string type, GameEventDelegate data)
		{
			List<GameEventDelegate> listeners = null;
			
			//remove listener if delegate exists
			if (_allEvents.TryGetValue(type, out listeners) && listeners.Count > 0)
			{
				if (listeners.Contains(data))
					listeners.Remove(data);
			}
			else
				Debug.Log("WARNING: Trying to remove event of type [" + type + "] that doesn't exist");
		}
	}
}
