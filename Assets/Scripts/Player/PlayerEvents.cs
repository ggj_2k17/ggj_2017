﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEvents : MonoBehaviour {
    public const string WEAPON_UPGRADE = "upgrade_gun";
    public const string BOMB_UPGRADE = "upgrade_bomb";
    public const string MOVE_UPGRADE = "upgrade_move";
    public const string ARMOR_UPGRADE = "upgrade_armor";
}
