﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class PlayerScript : MonoBehaviour
{

    [SerializeField] private GameObject Weapon;
    [SerializeField]
    private List<GameObject> weapons;
    [SerializeField]
    private List<GameObject> Bullets;
    private GameObject currentWeapon;
    private WeaponScript weaponScript;
    private float weaponreload;

    public enum BombTypes {
        None,
        Normal,
        Pulse,
        Bullet
    }

    public BombTypes bombType;


    private void SpawnWeapon()
    {
        currentWeapon = Instantiate(Weapon);
        currentWeapon.transform.position += transform.position;
        currentWeapon.transform.SetParent(transform);
        weaponScript = currentWeapon.GetComponent<WeaponScript>();
    }

    private void Aim()
    {
        weaponScript.Effect();
        if (Player.RightStickHorizontal > 0.19f || Player.RightStickHorizontal < -0.19f ||
            Player.RightStickVertical > 0.19f || Player.RightStickVertical < -0.19f)
        {
            Vector2 difference = new Vector2(Player.RightStickHorizontal, Player.RightStickVertical);
          //  print(difference);
            Vector3 eulerRot = currentWeapon.transform.rotation.eulerAngles;
           
            float angle = Vector2.Angle(new Vector2(1, 0), difference);
            if (Vector3.Cross(new Vector3(1,0,0),new Vector3(difference.x,-difference.y, 0)).z >0) angle = -angle;
            eulerRot.z = angle;
            currentWeapon.transform.rotation = Quaternion.Euler(eulerRot);
            currentWeapon.transform.position = transform.position + new Vector3(difference.normalized.x, difference.normalized.y, 0)/3;
        }
        if ((Player.RightTrigger > 0.19f || Player.Triggers > 0.19f || Input.GetKeyDown(KeyCode.A)) && weaponreload <= 0.0f ) {
            weaponScript.Shoot();
            weaponreload = weaponScript.GetWeaponSpeed()/100;
        }
    }

    void ReetOfFire()
    {
        if (weaponreload > 0.0f) weaponreload -= Time.deltaTime;
    }

    void DropBomb(BombTypes type) {
        switch (type) {
            case BombTypes.Normal:
                GameObject bomb = Instantiate(Resources.Load("Prefabs/Combat/Bombs/NormalBomb")) as GameObject;
                bomb.transform.position = transform.position;
                bomb.transform.rotation = transform.rotation;
                bomb.GetComponent<BaseBombScript>().playerId = (desiredPlayer == player.p1) ? 0 : 1;
                break;
            case BombTypes.Pulse:
                GameObject bomb2 = Instantiate(Resources.Load("Prefabs/Combat/Bombs/PulseBomb")) as GameObject;
                bomb2.transform.position = transform.position;
                bomb2.transform.rotation = transform.rotation;
                bomb2.GetComponent<BaseBombScript>().playerId = (desiredPlayer == player.p1) ? 0 : 1;
                break;
            case BombTypes.Bullet:
                break;
            default:
                break;
        }
    }

    void upgradeBomb(string type, object data)
    {
        if (desiredPlayer == player.p1 && (int)data == 1 || desiredPlayer == player.p2 && (int)data == 2)
        {
            int bom = Random.Range(1, 2);
            switch (bom)
            {
                case 1:
                    bombType = BombTypes.Normal;
                    break;
                case 2:
                    bombType = BombTypes.Pulse;
                    break;
//                case 3:
//                    newWep = weapons[2];
//                    break;

            }
        }
    }


    void upgradeWeapon(string type, object data)
    {
        if (desiredPlayer == player.p1 && (int)data == 1 || desiredPlayer == player.p2 && (int)data == 2)
        {
            Destroy(currentWeapon);
            GameObject newWep = new GameObject();
            GameObject newBul = new GameObject();

            int wapen = Random.Range(1, 3);
            switch (wapen)
            {
                case 1:
                    newWep = weapons[0];
                    break;
                case 2:
                    newWep = weapons[1];
                    break;
                case 3:
                    newWep = weapons[2];
                    break;

            }
            int bullets = Random.Range(1, 2);
            switch (bullets)
            {
                case 1:
                    newBul = Bullets[0]; break;
                case 2:
                    newBul = Bullets[0]; break;

            }
            int bulletsSize = Random.Range(1, 3);
            switch (bulletsSize)
            {
                case 1:
                    newBul.GetComponent<BulletScript>().BallSize = BulletScript.size.mini;
                    break;
                case 2:
                    newBul.GetComponent<BulletScript>().BallSize = BulletScript.size.normal;
                    break;
                case 3:
                    newBul.GetComponent<BulletScript>().BallSize = BulletScript.size.max;
                    break;
            }
            newWep.GetComponent<WeaponScript>().bullet = newBul;
            currentWeapon = Instantiate(newWep);
            currentWeapon.transform.position += transform.position;
            currentWeapon.transform.SetParent(transform);
            weaponScript = currentWeapon.GetComponent<WeaponScript>();
        }
    }
}
