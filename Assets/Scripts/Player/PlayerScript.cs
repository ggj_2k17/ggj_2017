﻿using System.Collections;
using System.Collections.Generic;
using System.Security.AccessControl;
using UnityEngine;

public partial class PlayerScript : MonoBehaviour {

    enum player { p1, p2}
    [SerializeField] private player desiredPlayer = player.p1;
    public PlayerController Player;
    private float nextActionTime = 0f;
    private float period = 2.2f;

	// Use this for initialization
	void Start () {
        body = GetComponent<Rigidbody2D>();
        collider = GetComponent<BoxCollider2D>();
	    if (desiredPlayer == player.p1) Player = Controller.P1;
        if (desiredPlayer == player.p2) Player = Controller.P2;
        setDefenceList();
        SpawnWeapon();
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        CheckGrounded();
        UpdateMovement();
        AlterGravity();
        Aim();
        ReetOfFire();
	}

    void Update() {
        if (Player.BButton) {
            if (bombType != BombTypes.None)
            {
                if(Time.time > nextActionTime)
                {
                    nextActionTime = Time.time + period;
                    DropBomb(bombType);
                }
            }
        }
    }
}
