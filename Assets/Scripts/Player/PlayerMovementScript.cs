﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OurLibrary.Events;

public partial class PlayerScript : MonoBehaviour {
    Rigidbody2D body;
    BoxCollider2D collider;
    [SerializeField]
    float speedMaxSpeedModifier = 1.5f;
    [SerializeField]
    float speed = 1;
    [SerializeField]
    float jumpHeight = 1;
    [SerializeField]
    float gravityValue = 9.81f;
    [SerializeField]
    float maxSpeed = 10;

    Vector2 gravity;

    EventDispatcher ed = EventDispatcher.GetInstance();

    bool grounded = false;
    bool canDoubleJump = false;
    bool doubleJumpEnabled = false;
    bool runningEnabled = false;
    bool controlsInvertedEnabled = false;

    private AnimatorControllerParameter[] parameters;

    void Awake()
    {
        ed.AddEventListener(PlayerEvents.MOVE_UPGRADE, upgradeMovement);
        ed.AddEventListener(PlayerEvents.ARMOR_UPGRADE, upgradeDefense);
        ed.AddEventListener(PlayerEvents.WEAPON_UPGRADE, upgradeWeapon);
        parameters = gameObject.GetComponentInChildren<Animator>().parameters;
        ed.AddEventListener(PlayerEvents.BOMB_UPGRADE, upgradeBomb);
    }

    void UpdateMovement()
    {
        Vector2 movement = Vector2.one;
        if (!controlsInvertedEnabled)
        {
             movement = new Vector2(Player.LeftStickHorizontal, Player.LeftStickVertical);
        }
        else
        {
             movement = new Vector2(-Player.LeftStickVertical, -Player.LeftStickHorizontal);
        }
        
         if (Player.LeftStickHorizontal < -0.19f || Player.LeftStickHorizontal > 0.19f ||
             Player.LeftStickVertical < -0.19f || Player.LeftStickVertical > 0.19f)
         {
            GetComponentInChildren<Animator>().SetFloat("speed", 1);
        }
         else
         {
            GetComponentInChildren<Animator>().SetFloat("speed", 0);
        }

        gravity = Physics2D.gravity.normalized;

        Vector2 force = new Vector2(Mathf.Abs(gravity.y) * movement.x, Mathf.Abs(gravity.x) * movement.y) * speed;
        force = force.normalized / 100 * speed;
        transform.position += new Vector3(force.x, force.y, 0);

        if (Player.AButton)
        {
            if (grounded)
            {
                body.AddForce(-gravity * jumpHeight);
                canDoubleJump = true;
                grounded = false;
                foreach (var defence in defenceList)
                {
                    defence.JumpDam();
                }
            }
            else
            {
                if(canDoubleJump && doubleJumpEnabled)
                {
                    canDoubleJump = false;
                    body.velocity = new Vector2(body.velocity.x, 0);
                    body.AddForce(-gravity * jumpHeight);
                    foreach (var defence in defenceList)
                    {
                        defence.JumpDam();
                    }
                }
            }
        }
    }

    void ChangeGravity(Vector2 newGravity) {
       // Physics2D.gravity = newGravity; ;
       // grounded = false;
      //  RotateTo(newGravity,1.0f);
    }

    public void RotateTo( float deltaTime) {
        Vector3 currentDown = -transform.up;
        Vector2 newDown =Physics2D.gravity;
        float angle = Vector2.Angle(newDown, currentDown);

        if (Vector3.Cross(new Vector3(currentDown.x,currentDown.y,0),new Vector3(newDown.x, newDown.y,0)).z < 9.81f){
        angle = -angle;
        }
        StartCoroutine(RotatePlayer(deltaTime,10,angle));
    }

    IEnumerator RotatePlayer(float deltaTime, int steps, float angle) {
        for(int i = 0; i <steps ; i++ ){
            transform.Rotate(new Vector3(0, 0, 1), angle / steps);
            yield return new WaitForSeconds(0.01f);
        }
       
    }
    void AlterGravity() {
        if (Input.GetKeyDown(KeyCode.UpArrow)) {
            ChangeGravity(new Vector2(0, gravityValue));
        }
        if (Input.GetKeyDown(KeyCode.DownArrow)) {
            ChangeGravity(new Vector2(0, -gravityValue));
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow)) {
            ChangeGravity(new Vector2(-gravityValue, 0));
        }
        if (Input.GetKeyDown(KeyCode.RightArrow)) {
            ChangeGravity(new Vector2(gravityValue, 0));
        }
    }

    void CheckGrounded() {
        if (!grounded) {
            Debug.DrawRay(transform.position, gravity.normalized * (gravity.x >0 ? collider.bounds.extents.x : collider.bounds.extents.y) * 1.1f, Color.black);
            RaycastHit2D[] hits = new RaycastHit2D[5];
            if (collider.Raycast(gravity.normalized, hits, (gravity.x > 0 ? collider.bounds.extents.x : collider.bounds.extents.y) + 0.03f) > 0) {
                grounded = true;
            }
        }
    }

    //testen wachten op robin
    void upgradeMovement(string type, object data)
    {
        if (desiredPlayer.ToString().Equals("P1") && (int) data == 1 || desiredPlayer.ToString().Equals("P2") && (int)data == 2)
        {
             if( !runningEnabled && !doubleJumpEnabled && !controlsInvertedEnabled)
             {
                 int rnd = Random.Range(1, 3);
                 switch (rnd)
                 {
                    case 1:
                        doubleJumpEnabled = true;
                        break;
                    case 2:
                        runningEnabled = true;
                        maxSpeed = speedMaxSpeedModifier * maxSpeed;
                        speed = speedMaxSpeedModifier * speed;
                        break;
                    case 3:
                         controlsInvertedEnabled = true;
                         break;
                 }
            }else if (runningEnabled && !doubleJumpEnabled && !controlsInvertedEnabled)
            {
                int rnd = Random.Range(1, 2);
                switch (rnd)
                {
                    case 1:
                        doubleJumpEnabled = true;
                        break;
                    case 2:
                        controlsInvertedEnabled = true;
                        break;
                }
            }
            else if (!runningEnabled && doubleJumpEnabled && !controlsInvertedEnabled)
            {
                int rnd = Random.Range(1, 2);
                switch (rnd)
                {
                    case 1:
                        controlsInvertedEnabled = true;
                        break;
                    case 2:
                        runningEnabled = true;
                        maxSpeed = speedMaxSpeedModifier * maxSpeed;
                        speed = speedMaxSpeedModifier * speed;
                        break;
                }
            }
            else if (!runningEnabled && !doubleJumpEnabled && controlsInvertedEnabled)
            {
                int rnd = Random.Range(1, 2);
                switch (rnd)
                {
                    case 1:
                        doubleJumpEnabled = true;
                        break;
                    case 2:
                        runningEnabled = true;
                        maxSpeed = speedMaxSpeedModifier * maxSpeed;
                        speed = speedMaxSpeedModifier * speed;
                        break;
                }
            }
            else if (!runningEnabled && doubleJumpEnabled && controlsInvertedEnabled)
            {
                runningEnabled = true;
                maxSpeed = speedMaxSpeedModifier * maxSpeed;
                speed = speedMaxSpeedModifier * speed;
                ed.RemoveEventListener(PlayerEvents.MOVE_UPGRADE, upgradeMovement);
            }
            else if (runningEnabled && !doubleJumpEnabled && controlsInvertedEnabled)
            {
                doubleJumpEnabled = true;
                ed.RemoveEventListener(PlayerEvents.MOVE_UPGRADE, upgradeMovement);
            }
            else if (runningEnabled && doubleJumpEnabled && !controlsInvertedEnabled)
            {
                controlsInvertedEnabled = true;
                ed.RemoveEventListener(PlayerEvents.MOVE_UPGRADE, upgradeMovement);
            }
        }
    }
};

