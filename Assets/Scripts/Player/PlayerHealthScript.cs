﻿using System.Collections;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using OurLibrary.Events;
using RandomGame.UI;
using UnityEngine;

public partial class PlayerScript : MonoBehaviour
{

    [SerializeField]
    private int health;
    private List<DefenceScript> defenceList = new List<DefenceScript>();
    bool thornEnabled = false;
    bool jumpDmg = false;
    bool defenseUpgrade = false;

    private void setDefenceList()
    {
        var tempDefenceList = GetComponents<DefenceScript>();
        foreach (var script in tempDefenceList)
        {
            defenceList.Add(script);
        }
    }

    public void AddDefence(DefenceScript script)
    {
        defenceList.Add(script);
    }

    public void RemoveDefence(DefenceScript script)
    {
        defenceList.Remove(script);
    }

    public void DealDamage(int damage)
    {
        foreach (var defence in defenceList)
        {
            damage = defence.dealDamage(damage);
        }
        health -= damage;
        if (health <= 0.0f) death();
        HUDEvents.HealhtObject healthChange = new HUDEvents.HealhtObject((int)Player.type, health);
        ed.Dispatch(HUDEvents.HEALTH_CHANGED, healthChange);
    }

    private void death()
    {
        print("PLayer " + desiredPlayer + " died");
        GameObject.Find("GameManager").GetComponent<GameManager>().IncreaseScore((desiredPlayer == player.p1) ? 0 : 1);
        health = 500;
    }

    void upgradeDefense(string type, object data)
    {
        if (desiredPlayer == player.p1 && (int)data == 1 || desiredPlayer == player.p2 && (int)data == 2)
        {
            if (!thornEnabled && !jumpDmg && !defenseUpgrade)
            {
                int rnd = Random.Range(1, 3);
                switch (rnd)
                {
                    case 1:
                        thornEnabled = true;
                        gameObject.AddComponent<ThornDamageScript>();
                        break;
                    case 2:
                        jumpDmg = true;
                        gameObject.AddComponent<JumpDamageScript>();
                        break;
                    case 3:
                        defenseUpgrade = true;
                        gameObject.AddComponent<DefenceScript>();
                        break;
                }
            }else if (thornEnabled && !jumpDmg && !defenseUpgrade)
            {
                int rnd = Random.Range(1, 2);
                switch (rnd)
                {
                    case 1:
                        defenseUpgrade = true;
                        gameObject.AddComponent<DefenceScript>();
                        break;
                    case 2:
                        jumpDmg = true;
                        gameObject.AddComponent<JumpDamageScript>();
                        break;
                }
            }
            else if (!thornEnabled && jumpDmg && !defenseUpgrade)
            {
                int rnd = Random.Range(1, 2);
                switch (rnd)
                {
                    case 1:
                        defenseUpgrade = true;
                        gameObject.AddComponent<DefenceScript>();
                        break;
                    case 2:
                        thornEnabled = true;
                        gameObject.AddComponent<ThornDamageScript>();
                        break;
                }
            }
            else if (!thornEnabled && !jumpDmg && defenseUpgrade)
            {
                int rnd = Random.Range(1, 2);
                switch (rnd)
                {
                    case 1:
                        thornEnabled = true;
                        gameObject.AddComponent<ThornDamageScript>();
                        break;
                    case 2:
                        jumpDmg = true;
                        gameObject.AddComponent<JumpDamageScript>();
                        break;
                }
            }
            else if (thornEnabled && jumpDmg && !defenseUpgrade)
            {
                thornEnabled = true;
                gameObject.AddComponent<ThornDamageScript>();
                ed.RemoveEventListener(PlayerEvents.ARMOR_UPGRADE, upgradeDefense);
            }
            else if (!thornEnabled && jumpDmg && defenseUpgrade)
            {
                jumpDmg = true;
                gameObject.AddComponent<JumpDamageScript>();
                ed.RemoveEventListener(PlayerEvents.ARMOR_UPGRADE, upgradeDefense);
            }
            else if (thornEnabled && !jumpDmg && defenseUpgrade)
            {
                defenseUpgrade = true;
                gameObject.AddComponent<DefenceScript>();
                ed.RemoveEventListener(PlayerEvents.ARMOR_UPGRADE, upgradeDefense);
            }
        }
    }



}
