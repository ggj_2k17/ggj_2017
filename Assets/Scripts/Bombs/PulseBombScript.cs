﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PulseBombScript : BaseBombScript {

    // Use this for initialization
    void Start() {
        base.Start();
    }

    // Update is called once per frame
    void Update() {

    }

    override public void Explode() {
        foreach (Rigidbody2D body in rigidbodies) {
            if (body != null) {
                body.AddForce(new Vector2(body.gameObject.transform.position.x - transform.position.x, body.gameObject.transform.position.y - transform.position.y).normalized * knockback,ForceMode2D.Force);
            }
            if (body.CompareTag("player1") || body.CompareTag("player2"))
            {
                body.GetComponent<PlayerScript>().DealDamage(Mathf.FloorToInt(damage));
            }
        }

        Destroy(this.gameObject);
    }

}
