﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseBombScript : MonoBehaviour {

    [SerializeField] protected int damage;
    [SerializeField] protected float explodeTimer;
    [SerializeField] protected float knockback;
    protected List<Rigidbody2D> rigidbodies = new List<Rigidbody2D>();
    public int playerId;
    
	// Use this for initialization
	public virtual void Start () {
		Twirl twirl = Camera.main.GetComponents<Twirl>()[playerId];
		twirl.executeExplosion(this);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    virtual public void Explode() {

    }

    void OnTriggerEnter2D(Collider2D other) {
        Rigidbody2D body = other.attachedRigidbody;
        if (body && !rigidbodies.Contains(body)) {

            rigidbodies.Add(body);
        }
    }

    void OnTriggerExit2D(Collider2D other) {
        Rigidbody2D body = other.attachedRigidbody;
        if (body && rigidbodies.Contains(body)) {
            rigidbodies.Remove(body);
        }
    }
}
