﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppleBombScript : BaseBombScript {

    // Use this for initialization
    public override void Start()
    {
        Invoke("Explode", 0.1f) ;
    }

    override public void Explode()
    {
        foreach (Rigidbody2D body in rigidbodies)
        {
            if (body != null)
            {
                float distance = Vector3.Distance(body.gameObject.transform.position, transform.position);
                body.AddForce(new Vector2(body.gameObject.transform.position.x - transform.position.x, body.gameObject.transform.position.y - transform.position.y).normalized * knockback / distance, ForceMode2D.Force);

                if (body.CompareTag("player1") || body.CompareTag("player2"))
                {
                    body.GetComponent<PlayerScript>().DealDamage(Mathf.FloorToInt(damage / distance));
                }
            }
        }

        Destroy(this.gameObject);
    }
}
