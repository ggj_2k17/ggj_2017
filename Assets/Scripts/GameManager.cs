﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using OurLibrary.Events;
using RandomGame.UI;

public class GameManager : MonoBehaviour {

    int player1Wins = 0;
    int player2Wins = 0;

    EventDispatcher dispatcher = EventDispatcher.GetInstance();

	// Use this for initialization
	void Start () {
        dispatcher.AddEventListener(HUDEvents.CLOSED_SHOP, ReloadScene);
	}

    public void IncreaseScore(int playerID) {
        if (playerID == 0) {
            player1Wins++;
        }
        else {
            player2Wins++;
        }

        if (player1Wins == 5 || player2Wins == 5) {
            SceneManager.LoadScene("Main Menu");
        }
        Time.timeScale = 0;
        dispatcher.Dispatch(HUDEvents.OPEN_SHOP, playerID + 1);

        //TODO update HUD
    }

    public void ReloadScene(string type, object data) {
        Destroy(GameObject.Find("Level Geometry"));
        GameObject level = Instantiate(Resources.Load("Prefabs/Map/Level Geometry")) as GameObject;
        level.transform.position = new Vector3(0.1f,5.1f,0);
        Time.timeScale = 1;

    }
	
}
