using System;
using UnityEngine;
using System.Collections;

    public class Vortex : ImageEffectBase
    {
        public Vector2 radius { get; set; }
        public float angle = 50;
        public Vector2 origin { get; set; }
		private Vector2 warpAmount;
		private int warpScalar = 2;

		private void Start()
		{
			origin = new Vector2(-0.5F, 1.5F);
			radius = new Vector2(.5F, 0.2F);
		}

		void OnRenderImage (RenderTexture source, RenderTexture destination)
        {
            ImageEffects.RenderDistortion (material, source, destination, angle, origin, radius);
        }

		public void executeWarp()
		{
			warpAmount = Physics2D.gravity.normalized / (warpScalar*20); // (0, -9.81)
			Camera.main.GetComponent<ScreenShakeScript>().ShakeScreen(.3f,1);
			if(warpAmount.x == 0.0f)
			{
				if(warpAmount.y > 0.0f) {
					origin = new Vector2(.5f, 0.0f);
					radius = new Vector2(.5F, 0.2F);
				}
				else
				{
					origin = new Vector2(.5f, 1.0f);
					radius = new Vector2(.5F, 0.2F);
				}
			}
			else {
				if(warpAmount.x > 0.0f) {
					origin = new Vector2(0.0f, 0.5f);
					radius = new Vector2(.1F, 0.5F);
				}
				else
				{
					origin = new Vector2(1.0f, 0.5f);
					radius = new Vector2(.1F, 0.5F);
				}
			}
			StopCoroutine("warpRoutine");
			StartCoroutine("warpRoutine");
		}


		IEnumerator warpRoutine ()
		{
			float startTime = Time.time;
			while (Time.time - startTime < warpScalar) 
			{
				origin += warpAmount ;
				yield return new WaitForFixedUpdate();
			}
		}
		
    }

