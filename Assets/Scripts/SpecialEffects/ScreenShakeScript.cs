﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenShakeScript : MonoBehaviour {

	// Use this for initialization
	// Update is called once per frame
	Vector3 originalPosition;

	void Start ()
	{
		originalPosition = this.gameObject.transform.localPosition;
	}

	public void ShakeScreen(float shakeAmount, float duration) {
		StartCoroutine(warpRoutine(shakeAmount, duration));
	}
	
	IEnumerator warpRoutine (float shakeAmount, float duration)
	{
		float startTime = Time.time;
		while (Time.time - startTime < duration) 
		{
			Vector3 temp = Random.insideUnitCircle * (shakeAmount/10);
			temp.z = -10;
			this.transform.localPosition = temp;
			yield return new WaitForFixedUpdate();
		}
		this.gameObject.transform.position = originalPosition;
	}
}
