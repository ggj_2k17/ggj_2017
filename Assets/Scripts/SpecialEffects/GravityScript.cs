﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityScript : MonoBehaviour {

	private Vortex vortex;
	private float counter = 0.0f;

	private List<Vector2> gravityList = new List<Vector2>();
	private int currentListID = 0;

	private float interval = 5.0f;
	private float minInterval = 3.0f;
	private float maxInterval = 5.0f;

    private PlayerScript playerScriptP1;
    private PlayerScript playerScriptP2;

	// Use this for initialization
	void Start () {
		vortex = Camera.main.GetComponent<Vortex>();
        playerScriptP1 = GameObject.Find("Player 1").GetComponent<PlayerScript>();
        playerScriptP2 = GameObject.Find("Player 2").GetComponent<PlayerScript>();
        gravityList.Add(new Vector2(0.0f, -9.81f));
		gravityList.Add(new Vector2(0.0f, 9.81f));
		gravityList.Add(new Vector2(9.81f, 0.0f));
		gravityList.Add(new Vector2(-9.81f, 0));
	}
	
	// Update is called once per frame
	void Update () {
		counter += Time.deltaTime;
		if(counter >= interval)
		{
			int result = Random.Range( 0, gravityList.Count-1 );
			if (result == currentListID) result += 1;
			currentListID = result;
			Physics2D.gravity = gravityList[currentListID];
            playerScriptP1.RotateTo(1.0f);
            playerScriptP2.RotateTo(1.0f);
            vortex.executeWarp();
			interval = Random.Range(minInterval, maxInterval);
			counter = 0.0f;
		}
	}
}
