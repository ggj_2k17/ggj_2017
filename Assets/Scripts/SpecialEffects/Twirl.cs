using System;
using System.Collections;
using UnityEngine;

public class Twirl : ImageEffectBase
{
    private Vector2 radius = new Vector2(0.3F,0.3F);
    [Range(0.0f,360.0f)]
    private float angle = 0;
	private float scale = .1f;
    private Vector2 center = new Vector2 (0.5F, 0.5F);
	BaseBombScript bomb;

	void Start ()
	{
		radius = Vector2.zero;
		angle = 0.0f;
	}

    // Called by camera to apply image effect
    void OnRenderImage (RenderTexture source, RenderTexture destination)
    {
        ImageEffects.RenderDistortion (material, source, destination, angle, center, radius);
    }

	public void executeExplosion(BaseBombScript _bomb) {
		bomb = _bomb;
		StartCoroutine(warpRoutine());
	}


	IEnumerator warpRoutine ()
	{
		bool temp = true;
		float startTime = Time.time;
		ScreenShakeScript shake = Camera.main.GetComponent<ScreenShakeScript>();
		while (Time.time - startTime < 1.75) 
		{
			if(bomb)
				center = Camera.main.WorldToViewportPoint(bomb.gameObject.transform.position);
			float duration = Time.time - startTime;
			if(duration <= 1f) {
				radius.x += Time.deltaTime * scale;
				radius.y += Time.deltaTime * scale;
			}
			else if(duration <= 1.25f) 
			{
				radius.x -= Time.deltaTime * scale*4;
				radius.y -= Time.deltaTime * scale*4;
			}
			else if(duration <= 1.5f) 
			{
				if(temp)
				{
					shake.ShakeScreen(1,1);
					bomb.Explode();
					temp = false;
				}
				radius.x += Time.deltaTime * scale*4;
				radius.y += Time.deltaTime * scale*4;
			}
			else if(duration <= 1.75f) 
			{
				radius.x -= Time.deltaTime * scale*4;
				radius.y -= Time.deltaTime * scale*4;
			}
			angle = Mathf.Lerp(0, 360, (Time.time - startTime)/2.75f);

			yield return new WaitForFixedUpdate();
		}
		radius = Vector2.zero;
	}
}

